import { FullConfig } from "@playwright/test";
const AdmZip = require("adm-zip");

async function globalSetup(config: FullConfig) {
    console.log("Report path " + config.rootDir);

    const reportPath = "/usr/src/app/tests/playwright-report";
    console.log("Report path: " + reportPath);

    var zip = new AdmZip();
    zip.addLocalFolder(reportPath, "./reports");
    zip.writeZip("./playwrightReport.zip")

}
export default globalSetup;