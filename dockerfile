# Use an official Node.js runtime as the base image
FROM node:latest

# Get the latest version of Playwright
FROM mcr.microsoft.com/playwright:v1.44.0-jammy

# Set the working directory in the container
WORKDIR /usr/src/app

# List contents of /usr/src/app
RUN ls -la /usr/src/app
# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install npm dependencies
RUN npm i

# Verify the contents of the directory (for debugging purposes)
RUN ls -al /usr/src/app
# Install Docker Compose
RUN apt-get update && \
    apt-get install -y docker-compose && \
    rm -rf /var/lib/apt/lists/*


# Copy the rest of the application code to the working directory
COPY . .

# Expose any ports the app is expecting
# EXPOSE <port>

# Command to run your tests using npm and Playwright
# CMD ["./entrypoint.sh"]
CMD ["npm","run","test"]
