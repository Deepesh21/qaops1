import globals from "globals";
import pluginJs from "@eslint/js";
import tseslint from "typescript-eslint";
import playwright from 'eslint-plugin-playwright'


export default [
  {languageOptions: { globals: globals.browser }},
  pluginJs.configs.recommended,
  ...tseslint.configs.recommended,
  {...playwright.configs['flat/recommended'],
  files: ['tests/**']},
   // Custom rules
   {
    rules: {
      "@typescript-eslint/no-explicit-any": "off", // Disable the rule globally
      // Add other custom rules here
      "no-console": "warn", // Add this line to highlight console.log statements
      "no-return-await": "warn", // Warn about unnecessary `return await`
      "playwright/no-useless-await": "warn", // Warn about unnecessary `await` expressions
    }
  }

];